from django.contrib.auth.models import AbstractUser
from django.db import models

# Create your models here.
from django.shortcuts import get_object_or_404
from django.urls import reverse

from Exams.models import ExamResult, Exam


class StudyGroup(models.Model):
    group_name = models.CharField(max_length=10, blank=False, null=False, verbose_name=u'Имя группы')

    def __str__(self):
        return self.group_name

    def get_update_url(self):
        return reverse('StudyGroupUpdate', kwargs={'pk': self.id})

    def get_delete_url(self):
        return reverse('StudyGroupDelete', kwargs={'pk': self.id})

    def get_users_in_group(self):
        return UserAccount.objects.filter(study_group=self)

    def get_group_stats(self):
        return reverse('GroupStats', kwargs={'study_group': self.id})

    def get_avr_score_by_exam(self, exam):
        users = UserAccount.objects.filter(examresult__exam_id=exam.id, study_group=self).distinct()
        total = 0
        result = []
        for user in users:
            total += exam.get_user_max_score(user)
        result.append({
            'count': len(users),
            'avr': round(total / len(users), 2),
        })
        return result

    def get_user_in_group_by_exam(self, exam_id):
        return self.get_users_in_group().filter(examresult__exam_id=exam_id).distinct()

    def get_max_users_score_in_group_by_exam(self, exam_id):
        exam = get_object_or_404(Exam, id=exam_id)
        max_score = exam.get_max_score()
        users = self.get_user_in_group_by_exam(exam.id)
        stats = []
        for user in users:
            stats.append({
                'user': user,
                'user_score': exam.get_user_max_score(user),
                'max': max_score,
            })
        return stats

    class Meta:
        verbose_name = u'Учебная группа'
        verbose_name_plural = u'Учебные группы'


class UserAccount(AbstractUser):
    middle_name = models.CharField(max_length=20, blank=True, null=False, verbose_name=u'Отчество')
    study_group = models.ForeignKey(StudyGroup, on_delete=models.SET_NULL, null=True, blank=True)

    REQUIRED_FIELDS = ['email', 'first_name', 'last_name']

    def __str__(self):
        return self.username

    class Meta:
        verbose_name = u'Пользователь'
        verbose_name_plural = u'Пользователи'

    def get_absolute_url(self):
        return reverse('Profile', kwargs={"pk": self.id})

    def get_update_url(self):
        return reverse('ProfileUpdate', kwargs={'pk': self.id})

    def get_delete_url(self):
        return reverse('ProfileDelete', kwargs={'pk': self.id})
