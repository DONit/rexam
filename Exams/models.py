import re

from django.core.exceptions import ObjectDoesNotExist
from django.core.validators import MinValueValidator
from django.db.models import Model, CharField, TextField, DateField, ForeignKey, SET_NULL, CASCADE, ImageField, \
    BooleanField, IntegerField, DateTimeField, Max
from django.http import Http404
from django.urls import reverse


class Subject(Model):
    name = CharField(max_length=30, blank=False, null=False, verbose_name=u'Имя предмета')

    def __str__(self):
        return self.name

    def get_delete_url(self):
        return reverse('SubjectDelete', kwargs={'pk': self.id})

    def get_update_url(self):
        return reverse('SubjectUpdate', kwargs={'pk': self.id})

    class Meta:
        verbose_name = u'Предмет'
        verbose_name_plural = u'Предметы'


class Exam(Model):
    exam_header = CharField(max_length=30, blank=False, null=False, verbose_name=u'Заголовок экзамена')
    exam_description = TextField(blank=False, null=True, verbose_name=u'Описание экзамена')
    exam_date = DateField(auto_now_add=True, blank=False, null=True, verbose_name=u'Дата создания')
    exam_subject = ForeignKey(Subject, blank=False, null=True, on_delete=SET_NULL, verbose_name=u'Предмет')
    exam_author = ForeignKey('Accounts.UserAccount', blank=False, null=True, on_delete=SET_NULL, verbose_name=u'Автор')
    exam_time = IntegerField(blank=False, null=True, verbose_name=u'Время прохождения',
                             validators=[MinValueValidator(1, 'Время должно быть больше или равно 1-ой минуте')])

    def __str__(self):
        return self.exam_header

    def get_detail_url(self):
        return reverse('ExamDetail', kwargs={'pk': self.id})

    def get_delete_url(self):
        return reverse('ExamDelete', kwargs={'pk': self.id})

    def get_update_url(self):
        return reverse('ExamUpdate', kwargs={'pk': self.id})

    def get_add_question_url(self):
        return reverse('QuestionCreate', kwargs={'exam_id': self.id})

    def get_start_url(self):
        return reverse('ExamStart', kwargs={'exam_id': self.id})

    def get_question_set(self):
        if Question.objects.filter(question_exam=self.pk).exists():
            return Question.objects.filter(question_exam=self.pk)
        else:
            raise Http404("Вопросы по экзамену %s отсутствут" % self.exam_header)

    def get_max_score(self):
        score = 0
        for question in self.get_question_set():
            score += question.get_max_score()
        return score

    def get_user_max_score(self, user):
        score = ExamResult.objects.filter(user=user, exam=self).aggregate(Max('score'))['score__max']
        return score

    def verify_exam(self, form, user):
        score = 0
        for form_question in form.cleaned_data:
            # get question by html id question_(id)
            question = Question.objects.get(pk=re.findall('(\d+)', form_question)[0])
            stats = question.get_stat_model()
            # get truth answers for question
            question_truth_answers_id = question.get_truth_answers_id()
            question_score = 0
            # value = id answer
            if len(form.cleaned_data[form_question]) > len(question_truth_answers_id) or len(form.cleaned_data[form_question]) == 0:
                question_score = 0
            else:
                for value in form.cleaned_data[form_question]:
                    question_score = question_score + 1 if (int(value) in question_truth_answers_id) else question_score - 1

            score += question_score if question_score >= 0 else 0
            if score < question.get_max_score():
                stats.error_score += 1
                stats.save(force_update=True)
        exam_result = ExamResult(exam=self, score=score, user=user)
        exam_result.save(force_insert=True)
        return score

    class Meta:
        verbose_name = u'Экзамен'
        verbose_name_plural = u'Экзамены'


class Question(Model):
    question_text = TextField(blank=False, null=False, verbose_name=u'Текст вопроса')
    question_exam = ForeignKey(Exam, blank=False, null=False, on_delete=CASCADE, verbose_name=u'Вопрос к экзамену')
    question_image = ImageField(upload_to='question_images', blank=True, null=True, verbose_name=u'Изображение вопроса')

    def __str__(self):
        return self.question_text

    def get_detail_url(self):
        return reverse('QuestionDetail', kwargs={'pk': self.id})

    def get_add_answer_url(self):
        return reverse('AnswerCreate', kwargs={'question_id': self.id})

    def get_delete_url(self):
        return reverse('QuestionDelete', kwargs={'pk': self.id, 'exam_id': self.question_exam.id})

    def get_update_url(self):
        return reverse('QuestionUpdate', kwargs={'pk': self.id, 'exam_id': self.question_exam.id})

    def get_answers_set(self):
        if Answer.objects.filter(answer_question=self.id).exists():
            return Answer.objects.filter(answer_question=self.id)
        else:
            raise Http404("Ответы по вопросу %s отсутствуют" % self.question_text)

    def get_truth_answers_id(self):
        if self.get_answers_set().filter(answer_truth=True).exists():
            answers = self.get_answers_set().filter(answer_truth=True)
            answers_id = []
            for answer in answers:
                answers_id.append(answer.pk)
            return answers_id
        else:
            raise Http404("Ошибка обработки правильных ответов")

    def get_max_score(self):
        return len(self.get_truth_answers_id())

    def get_stat_model(self):
        try:
            stats = QuestionStats.objects.get(question=self)
        except ObjectDoesNotExist:
            QuestionStats.objects.create(question=self, error_score=0)
            stats = QuestionStats.objects.get(question=self)
        return stats

    def get_error_score(self):
        return self.get_stat_model().error_score

    class Meta:
        verbose_name = u'Вопрос'
        verbose_name_plural = u'Вопросы'
        db_table = 'exam_questions'


class Answer(Model):
    answer_text = TextField(blank=False, null=False, verbose_name=u'Текст ответа')
    answer_question = ForeignKey(Question, blank=False, null=False, on_delete=CASCADE, verbose_name=u'Ответ к вопросу')
    answer_image = ImageField(upload_to='answer_images', blank=True, null=True, verbose_name=u'Изображение ответа')
    answer_truth = BooleanField(blank=False, null=False, verbose_name=u'Правильный ответ?')

    def get_delete_url(self):
        return reverse('AnswerDelete', kwargs={'pk': self.id, 'question_id': self.answer_question.id})

    def get_update_url(self):
        return reverse('AnswerUpdate', kwargs={'pk': self.id, 'question_id': self.answer_question.id})

    def __str__(self):
        return self.answer_text

    class Meta:
        verbose_name = u'Ответ'
        verbose_name_plural = u'Ответы'
        db_table = 'exam_answers'


class ExamResult(Model):
    exam = ForeignKey(Exam, blank=False, null=False, on_delete=CASCADE, verbose_name=u'Экзамен')
    score = IntegerField(verbose_name=u'Кол-во правильных овтетов')
    user = ForeignKey('Accounts.UserAccount', blank=False, null=False, on_delete=CASCADE, verbose_name=u'Пользователь')
    exam_date = DateTimeField(auto_now_add=True, blank=False, null=False, verbose_name=u'Дата прохождения')

    class Meta:
        verbose_name = u'Результат'
        verbose_name_plural = u'Результаты'


class QuestionStats(Model):
    error_score = IntegerField(verbose_name=u'Кол-во ошибочных ответов')
    question = ForeignKey(Question, on_delete=CASCADE, verbose_name=u'Вопрос')
